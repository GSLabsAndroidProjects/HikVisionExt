# HikVisionExt

[![](https://jitpack.io/v/com.gitlab.GSLabsAndroidProjects/HikVisionExt.svg)](https://jitpack.io/#com.gitlab.GSLabsAndroidProjects/HikVisionExt)

IP camera configuration SDK for Android.

- [Installation](#installation)
- [API](#api)
    - [Client](#client)
    - [Browser](#browser)
- [Examples](#examples)
    - [Activate](#activate)
    - [Connect to Wi-Fi](#connect-to-wi-fi)
    - [Enable DHCP on LAN interface](#enable-dhcp-on-lan-interface)
    - [Get camera info](#get-camera-info)
    - [Video/Audio codec](#videoaudio-codec)
    - [Date and time](#date-and-time)
    - [Motion detection](#motion-detection)

## Installation

build.gradle (Project)

```gradle
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
```

build.gradle (Module)

```gradle
dependencies {
    implementation 'com.gitlab.GSLabsAndroidProjects:HikVisionExt:1.18'
}
```

## API

The API consists of 2 main interfaces - Client and Browser.

### Client

`HveClient` class provides high level API for making HTTP requests to the camera - Activate, WirelessUpdate, IPAddressUpdate, etc. Each method has its synchronous and asynchronous version. Synchronous version ends with `Sync` suffix and blocks the current thread until request is completed. Asynchronous version ends with `Async` suffix and invokes a callback when request is completed. All callbacks are posted to the application's main thread.

```java
HveClient client = HveClient.client("http://192.168.8.1", "admin", "qwerty-123");
```

### Browser

`HveBrowser` class discovers the cameras in local network and resolves their IP addresses. Resolved addresses can be used by `HveClient` instances for making HTTP requests to the cameras.

```java
browser = HveBrowser.browser(this, "_psia._tcp");
browser.listeners.add(this);
browser.start();

public void endpointFound(HveBrowser sender, HveEndpoint endpoint) {
    HveClient client = HveClient.client(endpoint, "admin", "qwerty-123");
}
```

## Examples

First you need to reset the camera by holding the side button for 5 seconds. Then the camera will create an access point with parameters, specified on its body. You need to connect it using Settings application or `WifiManager` API.

### Activate

Most of request are protected with Digest auth. We need to specify the credentials for these challenges. Username is always `admin` so we only need to set the password up to 16 characters long. For security reasons the password should be encrypted. `V1` method does this encryption for you.

```java
HveActivateInfo activateInfo = new HveActivateInfo();
activateInfo.password = "qwerty-123";

client.activateV1Async(activateInfo, (responseStatus, exception) -> {
    if (responseStatus == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        // Success
    }
});
```

### Connect to Wi-Fi

Join the camera to the network named `My network` with password of `12345678`. Camera shuts down its access point on success.

```java
HveWPA wpa = new HveWPA();
wpa.algorithmType = "TKIP";
wpa.sharedKey = "12345678";
wpa.wpaKeyLength = wpa.sharedKey.length();

HveWirelessSecurity wirelessSecurity = new HveWirelessSecurity();
wirelessSecurity.securityMode = "WPA2-personal";
wirelessSecurity.wpa = wpa;

HveWireless wireless = new HveWireless();
wireless.enabled = true;
wireless.ssid = "My network";
wireless.wirelessSecurity = wirelessSecurity;

client.wirelessUpdateAsync("2", wireless, (responseStatus, exception) -> {
    if (responseStatus == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        // Success
    }
});
```

### Enable DHCP on LAN interface

Allow the camera to obtain dynamic address in local network instead of fixed one - `192.168.1.64`. This is required when multiple cameras are in the same network. To apply the changes reboot is required.

```java
HveIPv6Mode ipv6Mode = new HveIPv6Mode();
ipv6Mode.ipv6AddressingType = "dhcp";

HveIPAddress ipAddress = new HveIPAddress();
ipAddress.ipVersion = "dual";
ipAddress.addressingType = "dynamic";
ipAddress.ipv6Mode = ipv6Mode;

client.ipAddressUpdateAsync("1", ipAddress, (responseStatus, exception) -> {
    if (responseStatus == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        client.rebootAsync(null);
    }
});
```

### Get camera info

Get the information about camera such as name, model and serial number.

```java
client.deviceInfoGetAsync((deviceInfo, exception) -> {
    if (deviceInfo == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        Toast.makeText(this, deviceInfo.model, Toast.LENGTH_SHORT).show();
    }
});
```

### Video/Audio codec

Change the video stream encoding. Currently supported values are `H.264` and `H.265`.

```java
HveVideo video = new HveVideo();
video.videoCodecType = "H.264";
video.videoResolutionWidth = 1920;
video.videoResolutionHeight = 1080;
video.videoQualityControlType = "VBR";
video.maxFrameRate = 2500;

HveAudio audio = new HveAudio();
audio.enabled = true;
audio.audioCompressionType = "G.726";

HveStreamingChannel streamingChannel = new HveStreamingChannel();
streamingChannel.video = video;
streamingChannel.audio = audio;

client.streamingChannelUpdateAsync("101", streamingChannel, (responseStatus, exception) -> {
    if (responseStatus == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        // Success
    }
});
```

### Date and time

Change the camera screen time. `V1` method sets it to the client local time.

```java
client.timeUpdateV1Async((responseStatus, exception) -> {
    if (responseStatus == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        // Success
    }
});
```

### Motion detection

To disable motion detection use the following snippet.

```java
HveGrid grid = new HveGrid();
grid.rowGranularity = 18;
grid.columnGranularity = 22;

HveLayout layout = new HveLayout();
layout.gridMap = "fffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffcfffffc";

HveMotionDetectionLayout motionDetectionLayout = new HveMotionDetectionLayout();
motionDetectionLayout.sensitivityLevel = 40;
motionDetectionLayout.layout = layout;

HveMotionDetection motionDetection = new HveMotionDetection();
motionDetection.enabled = false;
motionDetection.grid = grid;
motionDetection.motionDetectionLayout = motionDetectionLayout;

client.motionDetectionUpdateAsync("1", motionDetection, (responseStatus, exception) -> {
    if (responseStatus == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        // Success
    }
});
```
