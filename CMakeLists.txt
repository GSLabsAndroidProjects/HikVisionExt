cmake_minimum_required(VERSION 3.10.2)
project(hikvisionext C)

set(CMAKE_C_STANDARD 99)

add_subdirectory(library-glibext/src/main/cpp)
add_subdirectory(library-soupext/src/main/cpp)
add_subdirectory(library-gnutlsext/src/main/cpp)
add_subdirectory(library-glibnetworkingext/src/main/cpp)
add_subdirectory(library-xmlext/src/main/cpp)
add_subdirectory(library-avahiext/src/main/cpp)
add_subdirectory(library-hikvisionext/src/main/cpp)
